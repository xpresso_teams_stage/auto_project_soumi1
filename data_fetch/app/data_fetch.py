MOUNT_PATH = None
import sys
import types
import pickle
import marshal
import traceback
import pandas as pd
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
## $xpr_param_component_name = data_fetch
## $xpr_param_component_type = pipeline_job
## $xprparam_componentflavor = python
