MOUNT_PATH = None
import sys
import types
import pickle
import marshal
import traceback
import pandas as pd
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
## $xpr_param_component_name = data_cleaner
## $xpr_param_component_type = pipeline_job
## $xprparam_componentflavor = python


def start(self, run_name):
     
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===

            train_data = pd.read_csv(self.train_file)
            test_data = pd.read_csv(self.test_file)

            train_data.columns = train_data.columns.str.replace('_', '-')
            test_data.columns = test_data.columns.str.replace('_', '-')

            if not train_data.isnull().values.any():
                train_data = train_data.dropna(axis=0)

            if not test_data.isnull().values.any():
                test_data = test_data.dropna(axis=0)

            train_data.to_csv(self.train_file, index=False)
            test_data.to_csv(self.test_file, index=False)

        except Exception:
            traceback.print_exc()
            self.completed(success=False)
        self.completed()
