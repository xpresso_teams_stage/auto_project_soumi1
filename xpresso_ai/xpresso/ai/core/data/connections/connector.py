""" Factory class for Data-Connectivity  """

__author__ = 'Shlok Chaudhari'
__all__ = 'connector'


import enum
import xpresso.ai.core.commons.exceptions.xpr_exceptions as xpr_exp
from xpresso.ai.core.data.connections.factory_fs import FSConnectorFactory
from xpresso.ai.core.data.connections.factory_db import DBConnectorFactory


class Datasources(enum.Enum):
    """

    Enum class that lists datasources supported by
    Xpresso Data-Connection library.

    """

    DB = "db"
    FS = "fs"


class Connector:
    """

    Factory class to provide Connector object of specified type.

    """

    @staticmethod
    def getconnector(user_datasource, datasource_type=None):
        """

        This method returns Connector object of a specific datasource.

        Args:
            datasource_type (str): attribute to specify "Local" or "BigQuery"
                to instantiate local filesystem or GCP BigQuery. Default is
                None, which invokes a distributed filesystem or database
                management module called Alluxio or Presto, respectively.
            user_datasource (str): a string object stating the
                                datasource ("FS" or "DB")

        Returns:
            object: Connector object

        """

        if user_datasource.lower() == Datasources.FS.value:
            return FSConnectorFactory().getconnector(datasource_type)
        elif user_datasource.lower() == Datasources.DB.value:
            return DBConnectorFactory().getconnector(datasource_type)
        else:
            raise xpr_exp.InvalidDictionaryException
